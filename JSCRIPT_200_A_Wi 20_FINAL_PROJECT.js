// Timing function to hide welcome screen after 5 seconds
setTimeout(function() {
	const welcomeScreen = document.getElementById('welcome');
	welcomeScreen.setAttribute('hidden', true)
}, 5000);

// Validate input for author first and last name. Simple validation here. More complex feedback based on API request.
const authorValidateBtn = document.getElementById('author-validate');
authorValidateBtn.addEventListener('click', function(e) {
	const regexName = /\w{2,}/
	const firstName = document.getElementById('first-name');
	const lastName = document.getElementById('last-name');

	let authorFormIsValid = undefined;

	if(regexName.test(firstName.value) === false) {
		authorFormIsValid = false;
		firstName.parentElement.querySelector('small').innerText = `Please enter at least two characters.`
	} else {
		authorFormIsValid = true;
		firstName.parentElement.querySelector('small').innerText = ``
	};

	if(regexName.test(lastName.value) === false) {
		authorFormIsValid = false;
		lastName.parentElement.querySelector('small').innerText = `Please enter at least two characters.`
	} else {
		authorFormIsValid = true;
		lastName.parentElement.querySelector('small').innerText = ``
	};


		if(!authorFormIsValid) {
			e.preventDefault();
			alert(`Author form is invalid.`)
		} else {
			e.preventDefault();
			alert(`Author form is valid.`)
		}		
});

const authorForm = document.getElementById('author-form');
authorForm.addEventListener('submit', function(e) {
  e.preventDefault();

  const firstName = document.getElementById('first-name').value;
  const lastName = document.getElementById('last-name').value;

  // Fetch reviews for author and add first 3 to page
  const BASE_URL = 'https://api.nytimes.com/svc/books/v3/reviews.json';

  const url = `${BASE_URL}?author=${firstName}+${lastName}&api-key=${API_KEY}`;

  fetch(url)
  	.then(function(data) {
    	return data.json();
  	})

  	.then(function(responseJson) {
    	//console.log(responseJson);

    	localStorage.setItem('review-result', JSON.stringify(responseJson));
    	console.log(localStorage.getItem('review-result'));

    	let firstThreeReviews = responseJson.results.slice(0, 3);
    	//console.log(firstThreeReviews);

    	for (let i = 0; i < firstThreeReviews.length; i++) {

    		const bookTitle = firstThreeReviews[i].book_title;
    		document.getElementById(`author-title${i}`).innerText = bookTitle;

    		const reviewAuthor = firstThreeReviews[i].byline;
    		document.getElementById(`author-author${i}`).innerText = `A review by ${reviewAuthor}`;

    		const reviewSummary = firstThreeReviews[i].summary;
    		document.getElementById(`author-description${i}`).innerText = reviewSummary;

    		const reviewUrl = firstThreeReviews[i].url;
    		document.getElementById(`author-review-link${i}`).href = reviewUrl;
    		document.getElementById(`author-review-link${i}`).target = '_blank'
    		document.getElementById(`author-review-link${i}`).innerText = `Read the full review`

    	}

    });

});


// Validate input for date fields.
const dateValidateBtn = document.getElementById('date-validate');
dateValidateBtn.addEventListener('click', function(e) {
	const regexTwoDigits = /\d{2}/;
	const regexFourDigits = /\d{4}/;
	const year = document.getElementById('year');
	const month = document.getElementById('month');
	const date = document.getElementById('date');

	let dateFormIsValid = undefined;

	if(regexFourDigits.test(year.value) === false) {
		dateFormIsValid = false;
		year.parentElement.querySelector('small').innerText = `Please enter a four digit number`
	} else {
		dateFormIsValid = true;
		year.parentElement.querySelector('small').innerText = ``
	};

	if(regexTwoDigits.test(month.value) === false) {
		dateFormIsValid = false;
		month.parentElement.querySelector('small').innerText = `Please enter a two digit number`
	} else {
		dateFormIsValid = true;
		month.parentElement.querySelector('small').innerText = ``
	};

	if(regexTwoDigits.test(date.value) === false) {
		dateFormIsValid = false;
		date.parentElement.querySelector('small').innerText = `Please enter a two digit number`
	} else {
		dateFormIsValid = true;
		date.parentElement.querySelector('small').innerText = ``
	};	

	if(!dateFormIsValid) {
		e.preventDefault();
		alert(`Date form is invalid.`)
	} else {
		e.preventDefault();
		alert(`Date form is valid.`);
	}

});

const dateForm = document.getElementById('date-form');
dateForm.addEventListener('submit', function(e) {
  e.preventDefault();

  const year = document.getElementById('year').value;
  const month = document.getElementById('month').value;
  const date = document.getElementById('date').value;

  // Fetch bestselling books for date and add top 3 to page
  const BASE_URL = 'https://api.nytimes.com/svc/books/v3/lists/';

  const url = `${BASE_URL}/${year}-${month}-${date}/hardcover-fiction.json?api-key=${API_KEY}`;

  fetch(url)
  	.then(function(data) {
    	return data.json();
  	})

  	.then(function(responseJson) {
    	//console.log(responseJson);

    	localStorage.setItem('date-result', JSON.stringify(responseJson));
    	console.log(localStorage.getItem('date-result'));

    	let firstThreeBooks = responseJson.results.books.slice(0, 3);
    	//console.log(firstThreeBooks);

    	for (let i = 0; i < firstThreeBooks.length; i++) {

    		const imageUrl = firstThreeBooks[i].book_image;
    		document.getElementById(`date-image${i}`).src = imageUrl;

    		const title = firstThreeBooks[i].title;
    		document.getElementById(`date-title${i}`).innerText = title;

    		const authorName = firstThreeBooks[i].author;
    		document.getElementById(`date-author${i}`).innerText = `By ${authorName}`;

    		const description = firstThreeBooks[i].description;
    		document.getElementById(`date-description${i}`).innerText = description;

    		const amazonLink = firstThreeBooks[i].amazon_product_url;
    		document.getElementById(`date-amazon${i}`).href = amazonLink;
    		document.getElementById(`date-amazon${i}`).target = '_blank'
    		document.getElementById(`date-amazon${i}`).innerText = `Buy on amazon`

    	}

	});

});


// Validate input for list field.
const listValidateBtn = document.getElementById('list-validate');
listValidateBtn.addEventListener('click', function(e){
	const listFormSelect = document.getElementById('list-select');

	let listFormIsValid = undefined;

	if (listFormSelect.value === 'choose') {
		listFormIsValid = false;
		listForm.parentElement.querySelector('small').innerText = `Please select an option.`;
	} else {
		listFormIsValid = true;
		listForm.parentElement.querySelector('small').innerText = ''
	}	

	if(!listFormIsValid) {
		e.preventDefault();
		alert(`List form is invalid.`)
	} else {
		e.preventDefault();
		alert(`List form is valid.`)
	}	
});

const listForm = document.getElementById('list-form');
listForm.addEventListener('submit', function(e) {
  e.preventDefault();

  const listName = document.getElementById('list-select').value;

  // Fetch bestselling books for date and add top 3 to page
  const BASE_URL = 'https://api.nytimes.com/svc/books/v3/lists/';

  const url = `${BASE_URL}/current/${listName}.json?api-key=${API_KEY}`;

  fetch(url)
  	.then(function(data) {
    	return data.json();
  	})

  	.then(function(responseJson) {
    	//console.log(responseJson);

    	localStorage.setItem('list-result', JSON.stringify(responseJson));
    	console.log(localStorage.getItem('list-result'));

    	let firstThreeBooks = responseJson.results.books.slice(0, 3);
    	//console.log(firstThreeBooks);

    	for (let i = 0; i < firstThreeBooks.length; i++) {

    		const imageUrl = firstThreeBooks[i].book_image;
    		document.getElementById(`list-image${i}`).src = imageUrl;

    		const title = firstThreeBooks[i].title;
    		document.getElementById(`list-title${i}`).innerText = title;

    		const authorName = firstThreeBooks[i].author;
    		document.getElementById(`list-author${i}`).innerText = `By ${authorName}`;

    		const description = firstThreeBooks[i].description;
    		document.getElementById(`list-description${i}`).innerText = description;

    		const amazonLink = firstThreeBooks[i].amazon_product_url;
    		document.getElementById(`list-amazon${i}`).href = amazonLink;
    		document.getElementById(`list-amazon${i}`).target = '_blank'
    		document.getElementById(`list-amazon${i}`).innerText = `Buy on amazon`


    	}

    });

});

