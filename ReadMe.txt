You will need an API key for the NYT books API for this final project.
https://developer.nytimes.com/docs/books-product/1/overview

Create a api-key.js file.
That file needs to contain a const API_KEY="your-api-key-here"

The project starts with a timing function that controls a welcome screen.

The first search fields allow to search the page based on author names. The page is populated with three reviews for books by said author.

The second search allows to search the hardcover-fiction bestseller list by date. The page is populated with the first three books for said date.

The third search allows to search a selection of four bestseller lists for the latest release. The page is populated with the first three books for the selected category.

All form fields are validated with page alert feedback via the validate button. This is done via regex functions.

The result of the API requests is used to set local storage for the respective category. I am only setting local storage at this point, not more.


The project includes the following four features:
- One or more timing functions
- One or more fetch requests to a 3rd party API
- Sets, updates, or changes local storage
- Contains form fields, validates those fields